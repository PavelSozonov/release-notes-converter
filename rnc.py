# Release Notes Converter

import argparse

class WorkItemGroup:

    def __init__(self, group_name):
        self.group_name = group_name
        self.work_items = []

    def add(self, work_item):
        self.work_items.append(work_item)

    def __str__(self):
        s = "## {}\n".format(self.group_name)
        for wi in self.work_items:
            s += str(wi) + "\n"
        return s

class WorkItem:

    def __init__(self, project, number, description):
        self.project = project
        self.number = number
        self.description = description

    def __str__(self):
        pattern = "* [{}-{}](https://jira.example.com/browse/{}-{}) - {}"
        return pattern.format(self.project, self.number, self.project, self.number, self.description)

class ReleaseNotes:

    def __init__(self, date, input, output):
        self.groups = []
        self.date = date
        self.input = input
        self.output = output
        self.parse()

    def parse(self):
        with open(self.input) as fp:
            line = fp.readline()
            while line:
                if line.strip() == "":
                    line = fp.readline()
                    continue
                if not line.startswith("["):
                    # Group Name
                    group_name = line.strip()
                    work_item_group = WorkItemGroup(group_name)
                    self.groups.insert(0, work_item_group)
                    #self.groups.append(work_item_group)
                else:
                    # Work Items
                    number = int(line.split("]")[0].split("-")[1])
                    prefix_len = len(line.split("]")[0]) + 4
                    description = line[prefix_len:].strip()
                    project = line.split("-")[0][1:]
                    work_item = WorkItem(project, number, description)
                    work_item_group.add(work_item)
                line = fp.readline()

    def __str__(self):
        s = "# Release Notes\n\nDate: {}\n\n".format(self.date)
        for group in self.groups:
            s += str(group) + "\n"
        return s.strip() + "\n"

    def save(self):
        with open(self.output, "w") as fp:
            fp.write(str(self))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--date", "-d", action="store", dest="date", help="release date (default: DD/Mon/YY)", default="DD/Mon/YY")
    parser.add_argument("--input", "-i", action="store", dest="input", help="input file name (default: input.txt)", default="input.txt")
    parser.add_argument("--output", "-o", action="store", dest="output", help="output file name (default: output.txt)", default="output.txt")
    args = parser.parse_args()
    ReleaseNotes(date=args.date, input=args.input, output=args.output).save()

