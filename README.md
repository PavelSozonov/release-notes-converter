# README

Converter from Jira default Release Notes output to Markdown

## Usage:
1. Create input.txt file and put Release Notes text from Jira inside
2. Run converter (use correct release date): python rnc.py -d "01/Jan/20"
3. Text in Markdown format will be written in output.txt file

To see help: python rnc.py -h